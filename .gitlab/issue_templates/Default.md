<!-- This is a template to report bugs, select the corresponding template to request a new feature. -->

Detailed description of the issue. Provide as much information as you can, potentially with images
or videos showing the issue.

## Steps to reproduce

1. Open Fractal
2. Enter a room
3. ...

## Information

* [ ] This bug is reproducible from the latest nightly build <!-- Check this box if the bug happens on Fractal's development version -->
* **Fractal Version**: <!-- The version of Fractal you were using when the bug occurred. Check the "About Fractal" dialog for this information -->
* **OS Version**: <!-- Operating system version, e.g. Fedora 36 -->
* **Installation Source**: <!-- Where you installed Fractal from, e.g. Flathub, GNOME Apps Nightly, AUR, or distro repositories -->
* **Homeserver**: <!-- The homeserver for your matrix account, e.g. matrix.org, gnome.org, … You can mention several of them if this is reproducible on multiple ones. -->

<!-- If you have error logs or a crash report, use the "Attach A File" button in the issue editor to attach it, or paste it in a code block below.

To access the logs, you can run `SYSTEMD_LESS=FRXMK journalctl -e -o cat _COMM=fractal`

To generate a stack trace in case of a crash, you can follow this guide: https://handbook.gnome.org/issues/stack-traces.html

To create a code block, surround the code with two lines with three "`" characters like so:

```
code goes here
```

-->
